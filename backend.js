var express = require('express');
var bodyParser = require('body-parser');
var app = express();

app.use(bodyParser.json());

var get = require('./json/GET.js');
var getAll = require('./json/GETALL.js');
var post = require('./json/POST.js');
var validate = require('./json/VALIDATE.js');
var put = require('./json/PUT.js');
var _delete = require('./json/DELETE.js');

/*
* Get All Users
*/
app.get('/api/users', function(req, res) {
  res.send(getAll());
});

/*
* Get User
*/
app.get('/api/user/:id', function(req, res) {
  res.send(get(req.params.id));
});

/*
* Post User
*/
app.post('/api/user', function(req, res) {
  res.send(post(req.body));
});

/*
* Put User
*/
app.put('/api/user', function(req, res) {
  res.send(put(req.body));
});

/*
* Delete User
*/
app.delete('/api/user/:id', function(req, res) {
  res.send(_delete(req.params.id));
});

/*
* Validate User
*/
app.post('/api/validate', function(req, res) {
  if (validate({ 'name': req.query.name, 'password': req.query.password }))
    res.sendStatus(200);
  else
    res.sendStatus(404);
});

app.listen(3000, function() {
  console.log('Backend online!');
});