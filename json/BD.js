var json = 
[
  {
    'id': 1,
    'name': 'Patrick Collier',
    'age': 22,
    'gender': 'Male',
    'password': '1234cinco'
  },
  {
    'id': 2,
    'name': 'Lindsay Bryan',
    'age': 25,
    'gender': 'Female',
    'password': '1234cinco'
  },
  {
    'id': 3,
    'name': 'Frankie Arnold',
    'age': 33,
    'gender': 'Male',
    'password': '1234cinco'
  },
  {
    'id': 4,
    'name': 'Ted Jones',
    'age': 12,
    'gender': 'Male',
    'password': '1234cinco'
  },
  {
    'id': 5,
    'name': 'Anne Lowe',
    'age': 48,
    'gender': 'Female',
    'password': '1234cinco'
  },
  {
    'id': 6,
    'name': 'Nicolas Wallace',
    'age': 27,
    'gender': 'Male',
    'password': '1234cinco'
  },
  {
    'id': 7,
    'name': 'Marta Gordon',
    'age': 75,
    'gender': 'Female',
    'password': '1234cinco'
  },
  {
    'id': 8,
    'name': 'Gloria Steele',
    'age': 19,
    'gender': 'Female',
    'password': '1234cinco'
  },
  {
    'id': 9,
    'name': 'Santiago Brooks',
    'age': 50,
    'gender': 'Male',
    'password': '1234cinco'
  }
];

function getNextId() {
  if (json.length === 0)
    return 1;

  var nextId; 

  for (var i = 0; i < json.length; i++) {
    if ((i + 1) == json.length) {
      nextId = i + 2;
    }
  }

  return nextId;
};

function getJson(id) {
  var temp = 'Not Found'; 

  for (var i = 0; i < json.length; i++) {
    if (json[i].id == id) {
      temp = json[i];
    }
  }

  return temp;
};

function getAllJson() {
  return json;
};

function putJson(obj) {
  if (!obj.id)
    return 'Not Found';

  var tempIndex = null;

  for (var i = 0; i < json.length; i++) {
    if (json[i].id == obj.id) {
      tempIndex = i;
    }
  }

  if (tempIndex !== null) {
    json[tempIndex].name = obj.name;
    json[tempIndex].age = obj.age;
    json[tempIndex].gender = obj.gender;
    json[tempIndex].password = obj.password;
  } else {
    return 'Not Found';
  }

  return json;
};

function postJson(obj) {
  var temp = {
    id: getNextId(),
    name: obj.name,
    age: obj.age,
    gender: obj.gender,
    password: obj.password
  };

  json.push(temp);

  return json;
};

function deleteJson(id) {
  for (var i = 0; i < json.length; i++) {
    if (json[i].id == id) {
      json.splice(i, 1);
    }
  }

  return json;
};

function validateUser(obj) {
  for (var i = 0; i < json.length; i++) {
    if ((json[i].name == obj.name) && (json[i].password == obj.password)) {
      return true;
    }
  }

  return false;
};

module.exports.get = function(id) {
  return getJson(id);
};

module.exports.getAll = function() {
  return getAllJson();
};

module.exports.post = function(obj) {
  return postJson(obj);
};

module.exports.validate = function(obj) {
  return validateUser(obj);
};

module.exports.put = function(obj) {
  return putJson(obj);
};

module.exports._delete = function(id) {
  return deleteJson(id);
};